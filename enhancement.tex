To further utilize the performance potential of the PCIe fabric, 
%Even though the baseline UT-SSD architecture presented above outperforms the traditional channel-based SSD (as will be demonstrated experimentally in Section~\ref{sec:eva-perf-comp}), we found that, in some cases, one may not be able to fully utilize the performance potential of the internal PCIe architecture. To address this, 
we propose two complementary architectural enhancements:  {\em FSB} and {\em EPB}. %While these advanced architectures can be implemented in any configuration of UT-SSD, we describe them based on the baseline UT-SSD (shown in Figure \ref{fig:ssd-arch}) in this paper.



\begin{figure}
\centering
\vspace{-15pt}
\subfloat[Baseline ]{\label{fig:fsb-arch-base}\rotatebox{0}{\includegraphics[scale=0.35]{fsb-arch-base}}}
\hspace{10pt}
\subfloat[FSB]{\label{fig:fsb-arch-apply}\rotatebox{0}{\includegraphics[scale=0.35]{fsb-arch-apply}}}
\vspace{-5pt}
\caption{The enhanced architecture with FSB utilizes neighbors' bandwidth by placing PCIe links across first layer switches.}
\label{fig:fsb-arch}
\vspace{-15pt}
\end{figure}






\subsection{First-layer Switch Bridging (FSB)}


\noindent \textbf{Problem:~} The proposed UT-SSD architecture consists of entirely-isolated multiple switch trees by the RPs of the RC. Since each RP's own bandwidth is determined by the number of physical lanes assigned to it, the aggregate bandwidth of the RC {\em cannot} be differentially redistributed over the available RPs. For example, in the baseline UT-SSD illustrated in Figure \ref{fig:ssd-arch} where there are 4 RPs and each RP has 32 (PCIe 3.x) lanes (an aggregate bandwidth of 128 GB/s), a  fixed 32 GB/s is allocated  to each of these RPs. In this case, these separated RPs and their corresponding PCIe links (between the first-layer SW and the connected RP) can experience different utilizations depending on the I/O pattern exhibited by the workload. For long sequential reads, the RPs and the connected links can be a bottleneck for the upsteams of read data, due to the form of the particular tree architecture adopted by UT-SSD. Recall that, 1024 flash chips are using the same RP to read out data. However, not all access patterns are perfectly sequential, which make the degree of congestion across switch trees and the utilizations of the RPs (actually, links to the RPs) different. As a result, some links (to the RP) can be quite busy, requiring more bandwidth, whereas the remaining links might under-utilize their fixed bandwidth. For example, based on our study of an HPC workload (\emph{eigen} in Table \ref{tab:workload-cha}), around 2,500 read data packets congest at a first-layer SW and wait for the release of PCIe link in a peak time duration. In contrast, another workload (\emph{cfs}) generates fewer data packets than \emph{eigen}, which causes the corresponding first-layer SW and the link remain idle. We refer to this scenario, which prevents us from maximizing the UT-SSD throughput, as the \emph{link under-utilization}.







%\begin{figure}
%\centering
%\includegraphics[width=0.95\linewidth, bb= 0 0 595 223]{fsb-arch}
%\vspace{-5pt}
%\caption{The enhanced UT-SSD architecture with FSB (on right) utilizes neighbors' link bandwidth by placing PCIe links among the first-layer switches.}
%\label{fig:fsb-arch}
%\vspace{-5pt}
%\end{figure}


\noindent \textbf{Proposed Solution:~} We propose FSB, which places PCIe links across the first layer SWs, as depicted in Figure \ref{fig:fsb-arch}. With FSB, when an SW becomes congested by data packets coming from the flash chips under its switch tree, it ``forwards'' some of them to another SW, if the UP and the link are under-utilized. By doing so, we can maximize the utilization of the ``aggregate bandwidth'' of the RC and achieve the maximum throughput in the ideal case. In the example scenario described in Figure \ref{fig:fsb-arch-base}, SW1 and SW3 suffer from the stacked data packets to be sent to the RC, whereas there is no packet to forward in SW2 and SW4, which results in the waste of the aggregate RC bandwidth in the baseline UT-SSD. When \emph{FSB} is applied (see Figure \ref{fig:fsb-arch-apply}), the stacked packets of SW1 and SW3 are redirected to the idle switches (SW2 and SW4) by using the bridges across the first layer SWs. As a result of making the links of SW3 and SW4 busy, the total SSD throughput can be further improved. Since the number of the first-layer SWs is small (as opposed to the number of lower switch layers), a full connection where there is a bridge between any pair of SWs is {\em not} expected to bring much overhead. In fact, in our baseline UT-SSD architecture, only six additional PCIe links are needed to implement the FSB. 

\noindent \textbf{Required Support:~} To implement FSB, the first-layer SWs need to know whether there is an idle SW to utilize the remaining bandwidth. In order to communicate the link availability information, we can use the data link layer packets (DLLPs). Unlike the I/O-related packets such as IORd, IOWr, Cpl, and CplD, DLLPs are typically exchanged between any two PCIe components -- on a need-basis or regularly -- either to acknowledge the I/O packet transmission or for traffic control purposes. Given that DLLPs are already available in the specification and are quite lightweight, we believe that FSB can be implemented in practice without much difficulty.


\begin{figure}
\centering
\vspace{-15pt}
\subfloat[Baseline ]{\label{fig:epb-arch-base}\rotatebox{0}{\includegraphics[scale=0.35]{epb-arch-base}}}
%\hspace{5pt}
\subfloat[EPB]{\label{fig:epb-arch-apply}\rotatebox{0}{\includegraphics[scale=0.35]{epb-arch-apply}}}
\vspace{-5pt}
\caption{The enhanced architecture with EPB utilizes idle flash chips by placing PCIe links between neighboring EPs.}
\label{fig:epb-arch}
\vspace{-15pt}
\end{figure}

%\begin{figure}
%\centering
%\includegraphics[width=0.97\linewidth, bb= 0 0 669 202]{epb-arch}
%\vspace{-5pt}
%\caption{The enhanced UT-SSD architecture with EPB (on right) utilizes idle flash chips by placing PCIe links between neighboring endpoints.}
%\label{fig:epb-arch}
%\vspace{-5pt}
%\end{figure}

\subsection{EndPoint Bridging (EPB)}

\noindent \textbf{Problem:~} In UT-SSD, the activity that contributes most to the latency is the ``memory operations'' performed by the flash chips. Compared to the data movement latency, the response time of flash chips takes much longer, due to the large unit size (page) of flash operations. In contrast, the PCIe bus path used for data movement is not a serious bottleneck because PCIe provides a sim-duplex type of data transfer, which allows the read- and write-data move over the PCIe links at the same time. We want to emphasize, however, that a lot of workloads have a tendency of repeatedly accessing the same set of flash chips, instead of exhibiting a pattern which distributes accesses over flash chips more or less evenly. These ``localized accesses'' to a small set of flash chips make other flash chips remain idle and forces a lot of accesses wait (stall) for the release of the busy flash chips. Letting many flash chips remain idle, which we call \emph{flash under-utilization}, in turn prevents us from exploiting the full throughput potential of UT-SSD. This flash under-utilization problem is frequently exhibited by a range of applications. For example, in \emph{hm}, one of our workloads, over 90\% of flash chips remain idle, while all its I/O requests access less than 10\% of the flash chips, shown in Figure \ref{fig:eval-flashutil-cfs-base}. Note that, although flash-status driven address remapping strategies \cite{ref-related-1, ref-space-2} were proposed to minimize the storage contention and maximize the storage utilization, they cannot be applied to UT-SSD since the RC, where I/O request packets are generated, cannot know the status of the flash chips which are distributed over the PCIe switch trees.  

\noindent \textbf{Proposed Solution:~} To address this flash under-utilization problem, we propose EPB, which places PCIe links between neighboring EPs, as depicted in Figure \ref{fig:epb-arch}. With this enhancement in place, when write requests and their associated data arrive at the target flash chip, if the target is busy in processing the previous request, the EP ``forwards'' the waiting writes to its neighbors. In the example scenario illustrated in Figure \ref{fig:epb-arch-base}, I/O requests flock into EP2 to access Flash2, whereas no access is present in the neighboring EP3 and Flash3. With the help of \emph{EPB} (see Figure~\ref{fig:epb-arch-apply}), the write request (Request4) can be redirected from EP2 to EP3 (note that EP1 fails to be the candidate since Flash1 is busy) by using the ``bridge'' between them, which in turn increases the flash utilization. While the ideal scenario is one in which one of the neighbors is idle, we can still get some benefit even if we need to forward the request to a remote (idle) EP. This is because the latency involved in the write-forwarding to a remote EP involves only a negligible (switching) overhead, compared to the long-latency flash write operations \cite{ref-flash-spec-micron}. In addition, an EP would not be overwhelmed to forward the request, since its main functionality is just passing the packet to the target device, {\em not} switching/routing the packets (by SWs). Note also that read requests cannot be moved to any other flash chips. As a result, Request3 in EP2 in Figure~\ref{fig:epb-arch-apply} is not sent to EP3.


\noindent \textbf{Required Support:~} Since EPB can write data in an arbitrary place where the flash is free, it is required to update the target data's physical address managed by the RC. To this end, we use an existing feature of the PCIe specification: the write completion packet Cpl which corresponds to the write request pakcets IOWr. Specifically, after a write operation is completed by the new target flash, it carries the new address information to the RC through the Cpl packet. According to the PCIe specification, the Cpl packet has many empty fields that can be used for the changed address of the write operation. In addition to the notification of the new address, the EPB also requires a mechanism to detect the free flash chips. Similar to the FSB, each EP can use DLLPs to notify the status of its own flash chip. When its flash chip remains idle for a certain period of time, the EP forwards light-weight DLLPs to its neighbors, which lets other EPs know the list of idle chips.
% chips and where to forward the stalled writes (if they have any).


















