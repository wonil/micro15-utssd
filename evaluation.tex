\subsection{Experimental Setup}

%\vspace{-5pt}
\begin{table}
\scriptsize
\vspace{-10pt}
\begin{tabular}{|p{28pt} | p{22pt}  | p{33pt} | p{33pt}  | p{30pt}  | p{35pt}  | }
\hline
\textbf{Workload} & \textbf{Read ratio (\%)} & \textbf{Read sequenetialness(\%)} & \textbf{Write sequentialness(\%)} & \textbf{Avg. request size (KB)} & \textbf{Avg. flash internal parallelism} \\
\hline \hline
cfs & 75.78 & 7.85 & 2.4 & 9.43 & 3.92 \\
\hline
fin & 21.27 & 4.18 & 6.98 & 4.14 & 0.57 \\
\hline
hm & 27.99 & 4.33 & 12.64 & 7.38 & 2.15 \\
\hline
mds & 94.78 & 5.24 & 0.28 & 56.47 & 3.21 \\
\hline
msnfs & 77.43 & 2.36  & 0.6 & 9.46 & 3.64 \\
\hline
prxy & 4.74 & 2.14 & 0.54 & 2.54 & 1.94 \\
\hline
web & 99.98 & 6.36 & 0 & 14.98 & 3.6 \\
\hline
websql & 99.11 & 21.29 & 0.32 & 47.68 & 3.11 \\
\hline
eigen & 99.71 & 82.47 & 0.2 & 127.52 & 3.7 \\
\hline
\end{tabular}
\vspace{-1pt}
\caption{Characteristics of our workloads.} %The maximum flash-internal parallelism is 4 in our baseline system.}
\label{tab:workload-cha}
\vspace{-15pt}
\end{table}


\noindent \textbf{Simulation Framework:~} To evaluate UT-SSD, we implemented a simulation framework for PCIe communication. Our framework models all PCIe components including RC, SW, EP and link, and simulates them at a cycle level, capturing all the PCIe-specific characteristics relevant to UT-SSD such as switching delay, data movement delay, and I/O request congestion. This framework is highly ``reconfigurable'', parameterized using a wide range of variables such as the number of lanes, switch tree layers, and component ports and lanes, allowing designers to construct different forms of data paths under the PCIe specification. %In addition, a flash model considering the chip internal parallelism is integrated into the framework for a complete UT-SSD simulation.


\noindent \textbf{Evaluated Configurations:~} Since the proposed architecture targets high-capacity SSDs over 1TB, we configured a 4TB UT-SSD based on 4096 1-GB flash chips as our {\em baseline.} As shown in Figure \ref{fig:ssd-arch}, this baseline UT-SSD constructs two-layer switch trees using the PCIe 3.x specification. In this architecture, a maximum of four pages can be simultaneously read from a two-die, two-plane flash with 4KB page size in a sequential access pattern. We also implemented in our simulator the two architectural enhancements discussed in Section~\ref{sec:ehnace}. For comparison purposes, we also modeled a conventional multi-channel SSD. To simulate the channel-based architecture, we modified the UT-SSD framework by reducing the number of switch tree layers to 1, minimizing the sizes of all kinds of buffers (in the SSD) to fit a flash transaction, and adjusting the bus performance to channel performance defined in ONFI 2.x \cite{ref-onfi-1}. Four different versions of the channel-based architecture, each with a different channel count, are tested. Thus, we evaluated a total of seven different SSD architectures: \emph{ch4}, \emph{ch8}, \emph{ch16}, \emph{ch32}, \emph{UT}, \emph{FSB}, and \emph{EPB}. All these configurations have the same storage capacity (4096 flash chips). To be able to accurately quantify the performance differences among the different SSD configurations tested, in our simulations, we fixed the software-related parameters across the configurations, such as the type of the I/O scheduler used and its queue size. %Specifically, we used a FIFO scheduler, which can be replaced, if desired, by more sophisticated schedulers. 


%\footnote{Even though there is limit in the number of flash chips in the multi-channel architecture shown in Table \ref{tab:arch-chip-num}, modern high-capacity SSDs keep increasing the number of flash chips and channels inside. For example, Micron's P420m \cite{ref-pciessd-micron-sheet} employs 32 channels and 512 flash dies (similar to the number of chips modeled in this paper) to fill a capacity of 1.4TB.}

%\noindent \textbf{Flash Software:~} To be able to accurately quantify the performance differences among the different SSD configurations tested, in our simulations, we fixed the software-related parameters across the configurations, such as the type of the I/O scheduler used and its queue size. Specifically, we used a FIFO scheduler, which can be replaced, if desired, by more sophisticated schedulers. 


\begin{figure}
\centering
\vspace{-15pt}
\includegraphics[width=0.6\linewidth]{eval-micro}
\vspace{-5pt}
\caption{Throughput of UT-SSD under sequential and random reads, and writes, with varying sizes.} %The enormous throughput values can be realized by the successful aggregation of the underlying 4096 flash chips' throughput.}
\label{fig:eval-micro}
\vspace{-15pt}
\end{figure}


\noindent \textbf{Tested Workloads:~} To evaluate the effectiveness of UT-SSD under different I/O access patterns, we used a variety of workloads, ranging from micro-benchmarks to HPC traces. The micro-benchmarks composed of sequential and/or random read and/or writes help us understand the general behavior of UT-SSD and its potential performance. Further, various enterprise workloads extracted from different application domains are used to quantify the additional benefits brought by UT-SSD over the channel-based SSD. The enterprise I/O traces we got from \cite{ref-trace1} and \cite{ref-trace2} are corporate mail server (\emph{cfs}), online transaction processing (\emph{fin}), hardware monitor (\emph{hm}), media server (\emph{mds}), remote file storage server (\emph{msnfs}), web proxy (\emph{prxy}), and search engines (\emph{web} and \emph{websql}). In addition, we ran an HPC workload (an Eigensolver application of nuclear dynamics simulation) obtained from the cluster of National Energy Research Scientific Computing Center \cite{ref-workload-ooc}. The evaluation using this HPC workload is important, since the proposed architecture targets many-chip SSDs and such high-capacity SSDs are likely to be the building blocks of the future high-performance storage systems. Table~\ref{tab:workload-cha} lists the important characteristics of the workloads.

% Note that, both the degree of sequentiality of I/O accesses and the degree of flash-level parallelism can significantly affect the performance of UT-SSD. This is because (i) the sequential accesses improve the utilization of flash chips by striping the I/O requests over them, and (ii) high levels of internal flash parallelism can maximize the benefits from the high-performance PCIe bus by transferring big chunks of data. 





\subsection{The Potential of UT-SSD}
%\vspace{-5pt}
To evaluate the general behavior and potential performance of UT-SSD, we executed {\em micro-benchmarks} with varying request sizes, read-write intensiveness, and sequentiality. Figure \ref{fig:eval-micro} shows the system throughput for the sequential reads (SR), sequential writes (SW), random reads (RR), and random writes (RW). It can be observed from this figure that the storage throughput is significantly improved under UT-SSD; in particular, SR with large request sizes achieves over 100GB/s. We further notice that, even under RW, our UT-SSD achieves over 10GB/s throughput, indicating that it \textit{successfully addresses the host interface under-utilization problem}. 

Overall, we gain two insights from this microbenchmark-based evaluation. First, UT-SSD achieves significant performance gains under the sequential I/O access patterns. This is because such patterns utilize intra-chip parallelism as well as inter-chip parallelism with the help of our address layout policy (i.e., address striping explained in Section~\ref{sec:model-sw-support}). Second, in general, higher performance is achieved when using larger I/O sizes. This is because, larger an I/O access is, more data the target flash produces/consumes by exploiting the intra-chip parallelism: die-level and plane-level parallelism.

% In other words, large volumes of data movement can get more benefit from the very high bandwidth provided by PCIe bus.


\begin{figure}
%\def\subfigcapskip{0pt}
\centering
%\vspace{-20pt}
\subfloat[Throughput]{\label{fig:eval-throughput}\rotatebox{0}{\includegraphics[width=0.5\linewidth]{eval-throughput}}}
%\hspace{-5pt}
\subfloat[IOPS]{\label{fig:eval-iops}\rotatebox{0}{\includegraphics[width=0.5\linewidth]{eval-iops}}}
\vspace{-5pt}
\caption{Throughput and IOPS of the channel-based architectures and UT-SSD architectures. \emph{UT} represents our baseline UT-SSD architecture without FSB/EPB.} %UT-SSD can achieve ultra-throughput by successfully aggregating the throughput of the underlying 4096 flash chips with PCIe bus.}
\label{fig:throughput-iops}
\vspace{-17pt}
\end{figure}


\begin{figure}
%\def\subfigcapskip{0pt}
\centering
\vspace{-10pt}
\subfloat[Average latency]{\label{fig:eval-lat}\rotatebox{0}{\includegraphics[width=0.5\linewidth]{eval-lat}}}
%\hspace{-5pt}
\subfloat[Average queue stall time]{\label{fig:eval-stall}\rotatebox{0}{\includegraphics[width=0.5\linewidth]{eval-stall}}}
\vspace{-5pt}
\caption{Average latency and queue stall time of the channel-based architectures and UT-SSD architectures.}
\label{fig:lat-stall}
\vspace{-10pt}
\end{figure}

\subsection{Performance Comparison}
\label{sec:eva-perf-comp}
%\vspace{-5pt}
\noindent \textbf{Throughput and IOPS:~} Figure \ref{fig:eval-throughput} plots the throughput for our seven different SSD architectures introduced earlier. As can be seen from this graph, {\em UT-SSD generates an order of magnitude higher throughput over the channel-based architectures,} with \emph{web} achieving up to 100GB/s. One can also see that, as the number of channels increases in the multi-channel architecture, the throughput also increases by reducing the number of flash chips for each shared channel and the average waiting time for the channel release. Thus, increasing the number of channels is likely to make the performance of the channel-based architecture approach to that of UT-SSD. Such a design, however, is not practical, because of the problems it brings (see Section \ref{sec:bg-mot1}). Further, for some workloads such as \emph{cfs} and \emph{hm}, UT-SSD can enjoy further throughput benefits (over the baseline UT-SSD) with the \emph{EPB} technique. According to our observations, these applications have a tendency of accessing a small set of flash chips very heavily, letting the remaining flash chips idle. EPB reduces the excessive pressure on the same flash chips by forwarding the waiting I/O requests to idle flash chips, further increasing the throughput. On the other hand, FSB achieves little (additional) performance gain over the baseline UT-SSD. This is primarily because the RPs (and its connected links) have sufficient bandwidth to process data without any delay whenever the data packets are present. A range of sequential reads can produce an excessive number of data packets, which can in turn form a bottleneck at the RP links. However, since the real workloads we tested are (i) a mixture of reads and writes and (ii) have random access patterns, congestions at the RPs are less likely. Instead, we found FSB to be more effective in the mixed-workloads (as will be discussed later). On the other hand, I/O Operations per Seconds (IOPS) are plotted in Figure \ref{fig:eval-iops}, which shows trends similar to the throughput.

 %The trends we noted with the throughput analysis can also be observed with these IOPS values.




%\noindent \textbf{IOPS:~} I/O Operations per Seconds (IOPS) of the seven SSD architectures evaluated are plotted in Figure \ref{fig:eval-iops}. In general, the trends we observed with the throughput analysis can also be observed with these IOPS values. While UT-SSD's throughput values with \emph{web} and \emph{websql} are similar each other, we see a better IOPS value with \emph{web}. This is because UT-SSD is able to process data in parallel even with small-sized I/O requests.   
%For example, since \emph{web} and \emph{websql} have very similar access patterns, they achieve almost the same throughput value in Figure \ref{fig:eval-throughput}. However, \emph{web} shows higher IOPS than \emph{websql}, because \emph{web} has much smaller request sizes than \emph{websql}, as indicated in Table~\ref{tab:workload-cha}.


\begin{figure}
\centering
%\vspace{-10pt}
\includegraphics[width=0.7\linewidth]{eval-multi}
\vspace{-5pt}
\caption{UT-SSD throughput under multi-workloads.}
\label{fig:eval-multi}
\vspace{-20pt}
\end{figure}

\noindent \textbf{Latency and Stall Times:~} Figure \ref{fig:lat-stall} plots the average I/O latency and the queue stall time of our seven SSD architectures. We observe a large reduction in latency when moving from the channel-based architectures to the UT-SSD architectures. This is mainly because the PCIe bus has extremely high data rate and two separate paths in both directions, whereas the traditional channel is slow and shared by a large number of flash chips. To better explain this, we present in Figure \ref{fig:eval-stall} the queue stall time, which captures the I/O wait time from the generation by the host to the start of the service. We can observe from these figures that a majority of workloads waste their time by waiting to get serviced in the multi-channel architectures. In contrast, the high-performance PCIe bus in UT-SSD prevents I/O requests from stalling at the queue, which in turn reduces the average latency by 77\%.


\begin{figure}
%\def\subfigcapskip{0pt}
%\hspace{15pt}
\centering
%\vspace{-10pt}
\subfloat[\emph{mw1}]{\label{fig:eval-fsb-mw1}\rotatebox{0}{\includegraphics[scale=0.32]{eval-fsb-mw1}}}
\hspace{15pt}
\subfloat[\emph{mw4}]{\label{fig:eval-fsb-mw4}\rotatebox{0}{\includegraphics[scale=0.32]{eval-fsb-mw4}}}
%s\vspace
\vspace{-5pt}
\caption{Changes in link utilization before and after FSB applied. Each link specifies "before (after) \%" and each bridge shows its data movement (KB/s).}
\label{fig:link-util}
\vspace{-15pt}
\end{figure}



\subsection{Multi-workloads on UT-SSD} 
%\vspace{-5pt}
Due to its high-capacity and the RP separation it employs, UT-SSD can be used by multiple different workloads ``at the same time''. Figure \ref{fig:eval-multi} shows the throughput of a few mixes of the workloads we tested earlier in isolation. From among the large number of potential combinations, we picked six read-intensive, representative combinations: \emph{\textbf{mw1}}\{\emph{cfs}, \emph{fin}, \emph{msnfs}, \emph{eigen1}\}, \emph{\textbf{mw2}}\{\emph{hm}, \emph{eigen2}, \emph{eigen3}, \emph{eigen4}\}, \emph{\textbf{mw3}}\{\emph{mds}, \emph{eigen2}, \emph{websql}, \emph{eigen4}\}, \emph{\textbf{mw4}}\{\emph{msnfs}, \emph{eigen1}, \emph{eigen2}, \emph{web}\}, \break \emph{\textbf{mw5}}\{\emph{eigen1},\emph{eigen2}, \emph{eigen3}, \emph{cfs}\}, and \emph{\textbf{mw6}}\{\emph{eigen1}, \emph{websql}, \emph{eigen3}, \emph{web}\}. One can see that the simultaneous runs of the different workloads on UT-SSD result in very good throughput values (over 30GB/s). Further, in contrast to the single application execution case, \emph{FSB} achieves more benefits. This is because the combined access patterns of the multiple co-running workloads introduce more conflicts and congested traffic from flash chips connected to a specific switch, compared to the single workload executions. Consequently, some first-layer SWs and their links can be under-utilized while others need more link bandwidth, which is the scenario where FSB can improve the UT-SSD throughput by forwarding over-crowded traffic on busy links to idle links. In contrast, the mixed workloads have little additional performance gain from \emph{EPB}, since only 1/4 of the total flash chips are assigned to each workload. This decreased share of flash chips for each workload reduces the number of under-utilized flash chips as well as chances for migrating the stalled write requests.









\subsection{Link Utilization}
%\vspace{-5pt}
To analyze the effectiveness of \emph{FSB} on multi-workloads, we measured the utilization of all links located between the first-layer SWs and the RC. Figure \ref{fig:link-util} shows the data movements (in KB/s) over the bridges of FSB, and the changes of link utilizations, in the cases of \emph{mw1} and \emph{mw4}. In Figure \ref{fig:eval-fsb-mw1}, \emph{eigen1}, which is part of \emph{mw1}, suffers from the link congestion caused by heavy traffic, whereas the other applications rarely utilize their links. On the other hand, after FSB is applied, \emph{eigen1}'s load is distributed over the others' under-utilized links by using the bridges. Note that the amount of data movement from the first-layer SW where \emph{eigen1} is serviced is much larger than the other data movements. As a result, the under-utilized links become busy, and UT-SSD can experience an improved link utilization and throughput. The same phenomenon is observed, but more dramatically, with \emph{mw4} in Figure \ref{fig:eval-fsb-mw4}. Since there are two read-intensive workloads (\emph{eigen1} and \emph{eigen2}), FSB can create more chances for forwarding excessive data and utilizing the idle links.

% FSB can improve throughput by creating chances for forwarding the waiting write requests and utilizing the idle links. 


\begin{figure}
\vspace{-20pt}
%\def\subfigcapskip{0pt}
\subfloat[\emph{cfs-base}]{\label{fig:eval-flashutil-cfs-base}\rotatebox{0}{\includegraphics[scale=0.3]{eval-cfs-base}}}
\hspace{-12pt}
\subfloat[\emph{cfs-EPB}]{\label{fig:eval-flashutil-cfs-epb}\rotatebox{0}{\includegraphics[scale=0.3]{eval-cfs-epb}}}
\hspace{-12pt}
\subfloat[\emph{hm-base}]{\label{fig:eval-flashutil-hm-base}\rotatebox{0}{\includegraphics[scale=0.3]{eval-hm-base}}}
\hspace{-12pt}
\subfloat[\emph{hm-EPB}]{\label{fig:eval-flash-hm-epb}\rotatebox{0}{\includegraphics[scale=0.3]{eval-hm-epb}}}
\vspace{-5pt}
\caption{Changes in flash utilization before/after EPB applied; cyan(-20\%), purple(21-40\%), green(41-60\%), red(61-80\%), and blue(81\%-).} %Numbers in the chart indicate the ratio of the number of flash chips with the corresponding utilization range to the total number of flash chips (4096).}
\label{fig:flash-util}
\vspace{-10pt}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{eval-epb-hop}
\vspace{-10pt}
\caption{The trace of forwarded write requests in terms of the number of EP hops.}
\label{fig:eval-epb-hop}
\vspace{-15pt}
\end{figure}


\subsection{Flash Utilization}
%\vspace{-5pt}
As discussed earlier, to further improve the throughput of UT-SSD, \emph{EPB} forwards write requests to idle flash chips and this helps to increase the flash utilization. Figure \ref{fig:flash-util} compares the flash utilization of two workloads (\emph{cfs} and \emph{hm}) ``before'' and ``after'' EPB is applied. The total 4096 flash chips of the UT-SSD are classified based on their ``utilization'' values. In the case of \emph{cfs}, shown in Figures \ref{fig:eval-flashutil-cfs-base} and \ref{fig:eval-flashutil-cfs-epb}, 93\% of the flash chips experience less than 40\% of utilization in the baseline UT-SSD. However, after EPB is applied, 97\% of the flash chips have at least 60\% utilization, which in turn increases the system throughput. The improvement in flash utilization is more dramatic in the case of \emph{hm}, where a majority flash chips are severely under-utilized in the baseline UT-SSD. To improve performance, EPB moves the write requests over multiple EPs, utilizing the otherwise idle flash chips (see Figure \ref{fig:eval-epb-hop}). One can see that most of the write requests are moved over hundreds of EP hops, since the neighboring flash chips are also busy (most of the time) due to I/O requests' locality. However, such a long-range forwarding is still beneficial because a write operation on the flash is too costly (several hundreds of microseconds to a few miliseconds); in contrast, transferring a page of data on PCIe bus is much cheaper (only 100ns for per-hop movement).




\subsection{SSD Size Sensitivity}
\label{sec:sensitivity}
The ultra-throughput achieved by UT-SSD architecture is due to the large number of flash chips in it. To analyze UT-SSD's potential for various size of SSDs, the number of flash chips inside an SSD is varied from 32 to 2048. Figure \ref{fig:cfs-sense} compares the throughput of different SSD architectures when \emph{cfs} runs. Similar trends are observed with other workloads as well. For small SSDs with 32 or 64 flash chips, the throughput achieved by UT-SSD is comparable to that of 32-channel based SSD. However, as the number of flash chips increases, UT-SSD outperforms multi-channel architectures. This shows that the PCIe inter-connection successfully aggregates the bandwidth of individual flash chips.






\begin{figure}
\centering
\vspace{-10pt}
\includegraphics[width=0.7\linewidth]{cfs-sense}
\vspace{-5pt}
\caption{\emph{Cfs} throughput achieved by SSDs with varying the number of flash chips.}
\label{fig:cfs-sense}
\vspace{-15pt}
\end{figure}

%%%%%


