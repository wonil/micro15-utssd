Over the past few years, Solid State Disk (SSD) architectures have undergone 
dramatic technological and architectural changes. As a result, today's high 
performance SSDs employ 64x more flash chips and internal buses (channels) 
than the first-generation SSDs \cite{ref-intro-complex-arch}, offering 24x better performance with much lower power consumption. This trend of employing an increasing number of flash chips and channels is expected to 
continue as SSDs are being increasingly accommodated on high-speed computer expansion buses like PCI Express (PCIe), instead of conventional storage interfaces such as SATA and SCSI \cite{ref-intro-pcie-trend1, ref-intro-pcie-trend2, ref-intro-pcie-trend3, ref-intro-pcie-trend4, ref-intro-pcie-trend5}. 





While the total bandwidth of the internal flash chips of modern SSDs 
can theoretically catch up with the PCIe transfer rates, we observe that most 
conventional PCIe-based SSDs {\em cannot} fully utilize their PCIe interface 
bandwidth. To show the significance of this performance issue, we studied the 
PCIe utilization trends of modern commercial SSDs, specifically, the performance 
difference between  the estimated ideal PCIe bandwidth (in the case of full 
thirty-two lanes) and the best SSD throughput (in the scenario of perfect 
sequential reads) extracted from their specifications \cite{ref-pciessd-ocz, ref-pciessd-micron, ref-pciessd-intel, ref-pciessd-fusionio, ref-pciessd-samsung, ref-pciessd-owc}. One can observe from Figure~\ref{fig:if-util} that 
most of the commercial PCIe-based SSD products (employing PCIe 2.x \cite{ref-if-pcie2}) 
are only able to utilize 20\% of the total PCIe bandwidth capacity, even in the 
ideal case.  To make matters worse, this low bandwidth utilization is expected 
to degrade even further, as the PCIe interface continues to improve -- the 
potential bandwidth of the latest PCIe 4.x interface approaches to 60 GB/s \cite{ref-if-pcie4}.


\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{if-util}
\vspace{-5pt}
\caption{The expected bus utilization of commercial PCIe-based SSD products.}
\label{fig:if-util}
\vspace{-20pt}
\end{figure}

\begin{figure}
\centering
\subfloat[Latency (ms)]{\label{fig:ch-lat}\rotatebox{0}{\includegraphics 
[width=0.45\linewidth] {ch-lat}}}   
\qquad
\subfloat[Throughput (MB/s)]{\label{fig:ch-thr}\rotatebox{0}{\includegraphics 
[width=0.45\linewidth]  {ch-thr}}}
\vspace{-5pt}
\caption{The performance of traditional channel-based SSD with varying number of 
channels and flash chips under sequential reads(SR) and random reads(RR).}
\vspace{-15pt}
 \label{fig:ch-perf}
\end{figure}



To identify the main reason behind this severe under-utilization problem, we 
simulated various PCIe-based SSD configurations that employ lots of flash chips 
(32$\sim$4096) through multiple shared channels (4$\sim$32). Figure~\ref{fig:ch-perf} plots the latency and throughput numbers of the simulated multi-channel 
architecture under both sequential reads (\emph{SR}) and random reads 
(\emph{RR}). As shown in the figure, the performance of the PCIe-based SSDs does {\em not} scale well with the increase in the number of flash chips. 
In fact, the latency significantly degrades as the number of flash chips is 
increased (Figure \ref{fig:ch-lat}), while the throughput saturates based on the channel bandwidth (Figure~\ref{fig:ch-thr}). There are two main reasons why a multi-channel SSD 
architecture cannot realize the cumulative performance potential of the 
underlying flash chips. First, the operating frequency of a channel can only be 
as fast as the conventional flash interface (at most tens \cite{ref-onfi-1} to hundreds MHz \cite{ref-onfi-2}), 
and second, a channel can be utilized by only a singe flash chip at a time because it is a shared resource. Consequently, the maximum bandwidth of the PCIe-SSDs cannot go beyond the intrinsic low channel bandwidth in the multi-channel architecture, which in turn causes the under-utilization problem discussed above. Furthermore, simply increasing the number of channels in a multi-channel architecture, which is currently the most popular approach in high performance SSDs \cite{ref-ch-1}, is not a promising option either. Specifically, increasing the number of channels increases both complexity and cost, primarily due to the extra logic needed such as the flash channel controller and data pins/wires required in each channel. Therefore, {\em to overcome the under-utilization problem and increase SSD performance, we need architectural innovations.} 


To fully utilize the bandwidth of PCIe interface, the total bandwidth of internal flash chips has to be significantly increased, which can be realized by integrating more and more flash chips inside a storage. Thus, instead of client SSDs which are based on tens of flash chips, \emph{we target high-capacity enterprise SSDs where over hundreds or thousands of flash chips are accommodated}. Note that high-capacity storage systems based on a very large number of flash chips in the form of ``all flash arrays" and ``SSD RAID" recently became popular in the HPC and cluster computing environments.




We note that (i) the data path between the host and SSD internal in the traditional channel-based architecture is separated into two parts, 
namely, the \emph{host interface} and the \emph{channel}, and (ii) the bottleneck is {\em not} the host interface, but the channel. 
Motivated by this observation, we ``unify'' the host interface and the channel into a single path of PCIe. 
By replacing the channels with native PCIe links in an SSD, we define a new enterprise SSD architecture, called {\bf Ultra-Throughput SSD} (UT-SSD), 
which can address the interface under-utilization problem of the conventional channel-based architectures. 
{\em UT-SSD does not only exhibit very high performance, but is also highly scalable from an architecture perspective,} allowing to scalably connect and integrate a very large number of flash chips. 
In this work, we present a ``design model'' for UT-SSD, instead of focusing on a specific instance of UT-SSD. 
Our proposed model can guide designers to construct a specific UT-SSD instance based on the needs of their applications.


Even though UT-SSD outperforms the traditional channel-based SSD, there exist 
scenarios, where one cannot achieve its full potential. In our experimental 
evaluations, we identified two such cases: (i) link under-utilization and (ii) flash under-utilization. 
Depending on the I/O access pattern of workloads, there could be uneven utilization of the bandwidth allocated
to flash-chips, resulting in some flash-chips needing more than their allocated bandwidth, while some needing less.
%requirements of a set of flash-chips can exceed the bandwidth 
%available to them, whereas other flash-chips in the same system can significantly 
%under-utilize their bandwidth share (note that the SSD interface bandwidth is 
%evenly distributed over sets of flash chips in the UT-SSD architecture; more on this later).
This uneven bandwidth distribution of UT-SSD architecture reduces the 
total interface (PCIe link) utilization, referred to as the \emph{link under-utilization} in this work. 
In addition, increasing the number of active flash chips can maximize UT-SSD's throughput. 
Many workloads with random access patterns, however, usually access hot flash chips while other flash chips remain idle, 
which prevents UT-SSD from utilizing its full bandwidth. This is referred to as the \emph{flash under-utilization} in this paper. 
We propose two architectural enhancements on top of our baseline UT-SSD, namely, First-layer Switch Bridging (FSB) 
and EndPoint Bridging (EPB), to address the link and flash under-utilization, respectively. 
Our main {\bf contributions} can be summarized as follows:


\noindent $\bullet$ \emph{A high-capacity scale-out SSD design:~} 
To fully utilize the bandwidth of PCIe interface, we propose a new enterprise SSD architecture, UT-SSD, \emph{by bringing PCIe inside an SSD}. By replacing the conventional channel by PCIe, the unified data path from flash chips to the host can significantly increase the SSD throughput, which in turn help us address the interface under-utilization problem. For an ambitious 4096-chip SSD configuration, the UT-SSD design can spend up to 90\% of PCIe bandwidth (for sequential reads) by successfully aggregating the bandwidth of 4096 flash chips.
In addition to its high performance, the inherent architectural scalability of PCIe allows one to integrate any number of flash chips inside an SSD. As a guideline to construct different UT-SSD instances, we present a \emph{tree-structured} ``design model'', based on PCIe specification. 



\noindent $\bullet$ \emph{An architectural solution to link under-utilization:~} 
%Depending on the access patterns exhibited by a workload, some flash chips may be very busy, while others are mostly idle. 
%This can make the links (between the host and sets of flash chips) under-utilized, wasting the link bandwidth of UT-SSD. 
To mitigate the uneven bandwidth utilization problem, we propose an architectural technique, called \emph{FSB}, that redistributes the 
unused bandwidth from the under-utilized links to the over-utilized links by bridging the first-layer switches. For the workloads 
having biased access patterns, FSB can achieve 62.7\% of the theoretical performance (throughput) of UT-SSD.


\noindent $\bullet$ \emph{An architectural solution to flash under-utilization.} 
Since the host system is unaware of the status of the underlying flash chips inside the SSD, most of the flash chips cannot be active all the time, 
referred to as the flash under-utilization problem. 
To further improve the UT-SSD throughput, we present another architectural enhancement, called \emph{EPB}. 
By bridging the endpoints where flash chips are attached, the issued write accesses can be forwarded to idle flash chips for faster processing. 
EPB brings an additional throughput improvement of 116\% by maximizing flash chip utilization.


%Overall, for a 4096-chip SSD design comparison, our proposed UT-SSD architecture can achieve 46x better performance over a conventional channel-based SSD, when all architectural optimizations mentioned above are on.