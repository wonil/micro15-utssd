\subsection{Conventional-channel Architecture}
\label{sec:bg-mot1}

In the traditional multi-channel architecture shown in Figure \ref{fig:ch-arch}, raw flash chips are attached to the underlying memory channels. To design high-capacity 
SSDs, more and more flash chips and channels can be added.


\noindent \textbf{Separate Data Paths:~} In this multi-channel architecture, the 
data path between the host and each flash chip is divided into two parts: 
\textit{channel} and \textit{host interface}. The channel is the path between the DRAM (in the SSD) and the flash chips (connecting to the channel), whereas the host interface is the bridge between the host system and the DRAM. When processing a read request, the data produced by a flash chip is transferred over the channel. After buffering it temporarily in the DRAM, the host interface controller sends 
the data to the host-side memory. In the reverse direction (on a write request) 
data are transferred to the target flash chip through the host interface and the 
channel. Note that the transmission capability of the two separate 
data paths are different, because they are ``physically'' different; while a channel adopts 40 $\sim$ 200 MB/sec narrow bus, a host interface employs multiple PCIe lanes, each offering 2 GB/sec.

%More specifically, an on-chip interconnect memory bus (e.g., AMBA \cite{ref-amba}) is used for the channel, whereas the host interface employs high-performance bus standard such as SATA and PCIe.


\noindent \textbf{Limitations of Channels:~} The underlying channel in the conventional SSD architecture has two critical 
limitations. First, compared to the extremely high-performance host interface, the channels have limited performance. More specifically, according 
to the flash interface protocol (ONFi 3.x \cite{ref-onfi-2}) and the 
specifications of flash memories, the channel adopts quite low frequencies such 
as 40MHz. As a result, it takes a non-trivial amount of time (up to 100 
us) to transfer a 4KB page of data over the channel. Second, a single channel is 
typically shared by many flash chips, which prevents a flash chip from 
using the channel most of the time.  When a flash chip uses the shared channel for data transfer, all other flash chips must wait until the channel is released (even though they have data ready to transfer), which reduces the throughput of the flash chips and increases their latency. This shortcoming becomes worse as the number of flash chips attached to a shared channel increases (which is the current trend in industry), as confirmed by our simulation results given in Figure \ref{fig:ch-perf}. Therefore, the improvement of one path (the host interface) without changing the other path (the channel) \textit{cannot} improve the overall SSD performance. This is why the current PCIe-based SSDs heavily \textit{under-utilize} the potential bandwidth of the PCIe host interface.


\begin{figure}
\centering
\vspace{-10pt}
\includegraphics[width=0.6\linewidth, bb= 0 0 603 237]{ch-arch}
%\vspace{-5pt}
\caption{The traditional channel-based SSD architecture where flash chips are 
attached to channels.}
\label{fig:ch-arch}
\vspace{-15pt}
\end{figure}




\subsection{PCI Express (PCIe)}
\label{sec:bg-mot2}
Among a wide range of bus standards, PCI Express (PCIe) is regarded as the most preferable bus architecture to replace the conventional embedded memory bus-based channel architecture from both the performance and design perspectives. 
%As a high-performance bus system, the PCIe's data rate and bandwidth are much better than the other bus architectures and AMBA-channels. 
In fact, the PCIe transfer rates are soon expected to improve even beyond 60 GB/s \cite{ref-if-pcie4}. Furthermore, with the support of sim-duplex data communication, data can travel in both directions at the same time, practically doubling its performance. Finally, PCIe is highly scalable from an architectural viewpoint, allowing the integration of thousands of flash chips. In PCIe, various components form hierarchical trees (as shown in Figure \ref{fig:pcie-com}), enabling designers to attach a large number of peripheral devices to it. As a result, any number of flash chips can be placed into an SSD using a combination of these PCIe components. The four main PCIe components, shown in Figure~\ref{fig:pcie-com}, can be summarized as follows.

\noindent \textbf{Root Complex (RC):~} The Root Complex is a PCIe component, where 
one side is directly connected to the host system and PCIe trees are attached to 
the other side. When the host sends data to the target devices, the data are 
first converted into a series of PCIe packets by the RC. The packetized data, destined to the target device, are injected into the PCIe tree through the root port. On the other hand, the data sent by target device and travelled over the PCIe tree arrive at the RC by using the root port in the reverse direction. The root ports, actually implemented as the interface (slot) of the host system, follow the PCIe's physical standard \cite{ref-if-pcie-rc}.



%The RC utilized in our work can be attached to the south bridge of the host system, since our purpose is to build a storage system.

\noindent \textbf{Endpoint (EP):~} Endpoint is a PCIe component to which 
peripheral devices can be attached, and is placed into the leaf position of 
PCIe trees. When data, in the form of PCIe packets, arrive at an EP (through the EP port), the EP first depacketizes it, and at this point, the interpreted data are ready to be used by the target device controller. The data sent by the device are packetized and inserted into the PCIe tree through the EP port. In the context of UT-SSD, each target device is a flash chip and all flash chips require their own EP. 



\noindent \textbf{Switch (SW):~} To connect multiple EPs to a single RC, a set of PCIe switches (in the form of a hierarchical tree) can be used. As illustrated in Figure~\ref{fig:pcie-com}, a switch can be a medium among the RC, EPs and other SWs. Note that SWs make the PCIe system scalable and expandable, meaning that any number of devices or the corresponding EPs can be connected to the single RC by using the switches in the form of a tree. A single switch has one up-port (UP) and multiple down-ports (DPs), and these ports are connected with one another. The UP can have a connection to a DP of an upper layer SW or the top layer RC. Similarly, each DP can be connected to the UP of a lower layer SW or the actual device port of EP. In our work, where a very large number of flash chips and the corresponding EPs are employed, multiple layers of PCIe switches are constructed as a big tree. According to the PCIe specification \cite{ref-if-pcie3}, all EPs are connected to a single SW by increasing the number of DPs to the number of EPs. However, such a design may not be the best option in terms of performance, as discussed later.



\begin{figure}
\centering
\vspace{-15pt}
\includegraphics[width=0.7\linewidth, bb= 0 0 583 310]{pcie-com}
%\vspace{-5pt}
\caption{The four PCIe components: root complex, endpoint, 
switch, and link, and an example connection.}
\label{fig:pcie-com}
\vspace{-18pt}
\end{figure}

\noindent \textbf{Link:~} To connect any pair of two components, a PCIe link is placed between them. According 
to the PCIe specification \cite{ref-if-pcie3}, a link consists of 1 to 32 lanes, 
which makes the link performance linearly scalable with the number of lanes 
inside. For example, PCIe 3.x defines a lane bandwidth as 1 GB/s; accordingly, a 
x4-lane link has 4 GB/s whereas x32-lane link has 32 GB/s. Note also that PCIe supports full-duplex communication.

%%%%%The physical constraint here is that both sides of the ports must support the same number of lanes in the link. 


